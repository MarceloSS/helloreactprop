import './App.css';

function Text(props) {
  return <h1> Ola <span style={{color: props.color}}>{props.value} </span></h1>
}

function App() {
  return (
    <div className="App">
      <header className="App-header">
        
        <Text color="#522d80" value="Marcelo"></Text>

      </header>
    </div>
  );
}

export default App;
